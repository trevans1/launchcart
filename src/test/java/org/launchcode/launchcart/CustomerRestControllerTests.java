package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CustomerRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Before
    public void createTestCustomers() {
        Customer cust1 = new Customer("test_user_1", "almightycatgod");
        customerRepository.save(cust1);
        Customer cust2 = new Customer("test_user_2", "uncrackablepassword");
        customerRepository.save(cust2);
    }

    @Test
    public void getAllCustomers() throws Exception{
        List<Customer> customers = customerRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/customers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(customers.size())));
        for(int i = 0; i < customers.size(); i++){
            res.andExpect(jsonPath("$[" + i + "].uid", is(customers.get(i).getUid())));
        }
    }

    @Test
    public void getSingleCustomer() throws Exception{
        Customer customer = customerRepository.findAll().get(0);
        mockMvc.perform(get("/api/customers/{id}", customer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(customer.getUid()));
    }

    @Test
    public void getNotFoundCustomer() throws Exception {
        mockMvc.perform(get("/api/customers/{id}", -1))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getCustomerCart() throws Exception {
        Customer customer = customerRepository.findAll().get(0);
        ResultActions res = mockMvc.perform(get("/api/customers/{id}/cart", customer.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(customer.getCart().getUid()));
    }



}
