package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by LaunchCode
 */
@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class AuthenticationControllerTests {

    @Autowired private MockMvc mockMvc;

    @Autowired private CustomerRepository customerRepository;

    private static final String testUserUsername = "launchcode";
    private static final String testUserPassword = "learntocode";
    private static int testUserUid;

    @Before
    public void setUpCustomer() throws Exception {
        Customer customer = new Customer(testUserUsername, testUserPassword);
        customerRepository.save(customer);
    }

    @Test
    public void testCanLogIn() throws Exception {
        mockMvc.perform(post("/login")
            .param("password", testUserPassword)
            .param("username", testUserUsername))
            .andExpect(status().is3xxRedirection())
            .andExpect(header().string("Location", ""));
    }

    @Test
    public void testCanViewRegistrationForm() throws Exception {
        mockMvc.perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Register")))
                .andExpect(content().string(containsString("<form")));
    }

    @Test
    public void testCanViewLoginForm() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Log In")))
                .andExpect(content().string(containsString("<form")));
    }

    @Test
    public void testCanRegister() throws Exception {
        String username = "newuser";
        String password = "abc123";
        mockMvc.perform(post("/register")
                .param("username", username)
                .param("password", password)
                .param("verifyPassword", password))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", ""));
        Customer customer = customerRepository.findByUsername(username);
        assertEquals(customer.getUsername(), username);
    }

    @Test
    public void testRegistrationFormChecksPasswords() throws Exception {
        mockMvc.perform(post("/register")
                .param("username", "newuser")
                .param("password", "password")
                .param("verifyPassword", "passord"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Passwords do not match")));
    }

    @Test
    public void testCanLogOut() throws Exception {
        mockMvc.perform(post("/login")
                .param("password", testUserPassword)
                .param("username", testUserUsername));
        mockMvc.perform(get("/logout"));
        mockMvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(header().string("Location", "/login"));
    }

}
