package org.launchcode.launchcart;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.models.Cart;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class CartRestControllerTests extends AbstractBaseRestIntegrationTest {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    MockMvc mockMvc;

    @Before
    public void createTestCarts() {
        Cart cart1 = new Cart();
        cart1.addItem(new Item("Test item 1", 1));
        cartRepository.save(cart1);
        Cart cart2 = new Cart();
        cart2.addItem(new Item("Test item 2", 2));
        cartRepository.save(cart2);
    }

    @Test
    public void getAllCarts() throws Exception{
        List<Cart> carts = cartRepository.findAll();
        ResultActions res = mockMvc.perform(get("/api/carts/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$", hasSize(2)));
        for(int i = 0; i < carts.size(); i++){
            res.andExpect(jsonPath("$[" + i + "].uid", is(carts.get(i).getUid())));
        }
    }

    @Test
    public void getSingleCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        mockMvc.perform(get("/api/carts/{id}", cart.getUid()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("uid").value(cart.getUid()));
    }

    @Test
    public void getNotFoundCart() throws Exception{
        mockMvc.perform(get("/api/carts/{id}", -1))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addItemToCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = new Item("New item", 3);

        cart.addItem(item);
        String json = json(cart);
        //cart directly affects the repository in memory so we have to return it to its original state before testing
        cart.removeItem(item);

        ResultActions res = mockMvc.perform(put("/api/carts/{id}", cart.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("items", hasSize(2)));
        List<Item> items = cart.getItems();
        for (int i = 0; i < items.size(); i++) {
            res.andExpect(jsonPath("items[" + i + "].uid", is(items.get(i).getUid())));
        }
    }

    @Test
    public void removeItemFromCart() throws Exception {
        Cart cart = cartRepository.findAll().get(0);
        Item item = cart.getItems().get(0);

        cart.removeItem(item);
        String json = json(cart);
        cart.addItem(item);

        ResultActions res = mockMvc.perform(put("/api/carts/{id}", cart.getUid())
                .content(json).contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("items", hasSize(0)));
    }
}
