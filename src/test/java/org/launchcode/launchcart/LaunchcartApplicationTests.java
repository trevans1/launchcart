package org.launchcode.launchcart;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.launchcode.launchcart.controllers.AuthenticationController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class LaunchcartApplicationTests extends AbstractBaseIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void homePageLoads() throws Exception {
		mockMvc.perform(get("/")
				.sessionAttr(AuthenticationController.customerSessionKey, testUserUid))
				.andExpect(status().isOk())
				.andExpect(content().string(containsString("LaunchCart")));
	}

	@Test
	public void redirectToLoginIfNotLoggedIn() throws Exception {
		mockMvc.perform(get("/"))
				.andExpect(status().is3xxRedirection())
				.andExpect(header().string("Location", "/login"));
	}

}
