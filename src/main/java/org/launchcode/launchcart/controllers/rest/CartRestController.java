package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.CartRepository;
import org.launchcode.launchcart.models.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/carts")
public class CartRestController {

    @Autowired
    CartRepository cartRepository;

    @GetMapping("")
    public List<Cart> getAllCarts(){
        return cartRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity getSingleCart(@PathVariable int id){
        Cart cart =  cartRepository.findOne(id);
        if(cart == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cart, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateCart(@PathVariable int id, @RequestBody Cart cart){
        if(id != cart.getUid()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        if(cartRepository.findOne(id) == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(cartRepository.save(cart), HttpStatus.OK);
    }

}
