package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("")
    public List<Customer> getAllCustomers(){
        return customerRepository.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity getSingleCustomer(@PathVariable int id){
        Customer cust = customerRepository.findOne(id);
        if(cust == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(cust, HttpStatus.OK);
    }

    @GetMapping("{id}/cart")
    public ResponseEntity getCustomerCart(@PathVariable int id){
        Customer cust = customerRepository.findOne(id);
        if(cust == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(cust.getCart(), HttpStatus.OK);
    }

}
