package org.launchcode.launchcart.controllers.rest;

import org.launchcode.launchcart.data.ItemRepository;
import org.launchcode.launchcart.models.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value="api/items")
public class ItemRestController {

    @Autowired
    ItemRepository itemRepository;

    @GetMapping("")
    public List<Item> getItems(@RequestParam Optional<Double> price,
                               @RequestParam(name="new") Optional<Boolean> newItem,
                               @RequestParam Optional<String> desc){
        //Why split it into streams multiple times when I can just have a more complex filter!
        List<Item> filtered = itemRepository.findAll().stream()
                .filter(item ->
                        (!price.isPresent() || item.getPrice() == price.get())
                            && (!newItem.isPresent() || item.isNewItem() == newItem.get())
                            && (!desc.isPresent() || item.getDescription().contains(desc.get()))
                            //&& more parameters!
                ).collect(Collectors.toList());

        return filtered;
    }

    @GetMapping("/{id}")
    public ResponseEntity getSingleItem(@PathVariable int id){
        Item item = itemRepository.findOne(id);
        if(item != null){
            return new ResponseEntity<>(item, HttpStatus.OK);
        }

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PostMapping("")
    public ResponseEntity createItem(@RequestBody Item item){
        return new ResponseEntity<>(itemRepository.save(item), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateItem(@PathVariable int id, @RequestBody Item item){
        if(itemRepository.findOne(id) == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(id != item.getUid()){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(itemRepository.save(item), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteItem(@PathVariable int id){
        if(itemRepository.findOne(id) == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        itemRepository.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
