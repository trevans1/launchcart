package org.launchcode.launchcart.controllers;

import org.launchcode.launchcart.data.CustomerRepository;
import org.launchcode.launchcart.models.Customer;
import org.launchcode.launchcart.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;

/**
 * Created by LaunchCode
 */
public abstract class AbstractCustomerController {

    @Autowired
    protected CustomerRepository customerRepository;

    @Autowired private HttpSession session;

    public static final String customerSessionKey = "user_id";

    public Customer getCustomerFromSession() {
        Integer userId = (Integer) session.getAttribute(customerSessionKey);
        return userId == null ? null : customerRepository.findOne(userId);
    }

    public void setUserInSession(Customer customer) {
        session.setAttribute(customerSessionKey, customer.getUid());
    }

    @ModelAttribute("customer")
    public User geCustomerForModel() {
        return getCustomerFromSession();
    }

}

